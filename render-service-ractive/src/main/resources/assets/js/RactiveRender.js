/* global Ractive, HUBU */

//Initialize Ractive.templates
(function(){
  "use strict";
  if(Ractive.templates === undefined){ Ractive.templates = {}; }
})();

/**
 * Implementation of the {@link RactiveRenderService} based on [Ractive]{@link http://ractivejs.org}
 *
 * @class RactiveRender
 * @extends HUBU.AbstractComponent
 * @implements {RactiveRenderService}
 * @implements {RenderService}
 */
function RactiveRender(){
  "use strict";

  var self = this;
  var _hub;
  var _ractive;

  var _model; //ractive data
  var _template; //ractive template
  var _container; //ractive el

  var _autorender = false; //render on start
  var _lazy = false; //lazy two way binding
  var _twoway = true; //two way binding

  self.name = "RactiveRenderDefault";

  self.getComponentName = function(){
    return self.name;
  };

  /**
   * Configure the instance of the RactiveRender component.
   *
   * @method configure
   * @memberof RactiveRender
   * @inner
   * @param {HUBU.hub} theHub - The HUBU hub.
   * @param  conf - The RactiveRender configuration.
   * @param {map} conf.model - The model link to the view to be render.
   * @param {string|function} conf.container - The dom element in wich the rendered view must be injected.
   * @param {string} conf.template - The Ractive template use in order to render the view.
   * @param {boolean} [conf.autorender=false] - If true the view will be render when this component start.
   * @param {boolean} [conf.lazy=false] - Lazy two way binding.
   * @param {boolean} [conf.twoway=true] - Activate/Desactivate two way binding between the view and the model.
   */
  self.configure = function(theHub,conf){
    _hub = theHub;

    if(typeof conf.model === "undefined"){
      throw "The `model` configuration property is mandatory.";
    }

    if(typeof conf.template === "undefined"){
      throw "The `template` configuration property is mandatory.";
    }

    if(typeof conf.container === "undefined"){
      throw "The `container` configuration property is mandatory.";
    }

    _model = conf.model;

    if(typeof conf.template === "string" && Ractive.templates[conf.template] !== undefined){
      _template = Ractive.templates[conf.template];
    } else{
      _template = conf.template;
    }

    _container = conf.container;

    if(typeof conf.name === "string"){
      self.name = conf.name;
    }

    if(typeof conf.autorender === "boolean"){
      _autorender = conf.autorender;
    }

    if(typeof conf.twoway === "boolean"){
      _twoway = conf.twoway;
    }

    if(typeof conf.lazy === "boolean"){
      _lazy = conf.lazy;
    }

    var props = {
      autorender : _autorender,
      twoway : _twoway,
      lazy : _lazy
    };

    if(typeof conf.properties === "object"){
      for(var prop in conf.properties){
        props[prop] = conf.properties[prop]; //extend
      }
    }

    //register the RactiveRenderService
    _hub.provideService({
      component : self,
      contract : window.RactiveRenderService,
      properties : props
    });

    //register the RenderService
    _hub.provideService({
     component : self,
     contract : window.RenderService,
     properties : props
    });
  };

  /** @inheritdoc */
  self.start = function(){
    if(_autorender){
      self.render();
    }
  };

  /** @inheritdoc */
  self.stop = function(){
    self.teardown();
    _ractive=undefined;
  };

  /** @inheritdoc */
  self.render = function(success){
    self.teardown(); // if already running

    _ractive = new Ractive({
      el: _container,
      template: _template,
      data: _model,
      oncomplete: success,
      lazy: _lazy,
      twoway: _twoway
    });
  };

  /** @inheritdoc */
  self.set = function(key, value, success){
    if(_ractive === undefined){
      HUBU.logger.debug("This ractive has not been rendered, you need to render it by calling render().");
      _model[key] = value;
      return;
    }

    _ractive.set(key, value, success);
  };

  /** @inheritdoc */
  self.update = function(key, success){
    if(_ractive === undefined){
      HUBU.logger.debug("This ractive has not been rendered, you need to render it by calling render().");
      return;
    }

    _ractive.update(key, success);
  };

  /** @inheritdoc */
  self.on = function(eventName, handler){
    if(_ractive === undefined){
      HUBU.logger.debug("This ractive has not been rendered.");
      return;
    }

    return _ractive.on(eventName, handler);
  };

  /** @inheritdoc */
  self.off = function(eventName, handler){
    if(_ractive === undefined){
      HUBU.logger.debug("This ractive has not been rendered.");
      return;
    }

    return _ractive.off(eventName, handler);
  };

  /** @inheritdoc */
  self.observe = function(key, observer, options){
    if(_ractive === undefined){
      HUBU.logger.debug("This ractive has not been rendered.");
      return;
    }

    return _ractive.observe(key, observer, options);
  };

  /** @inheritdoc */
  self.find = function(selector){
    if(_ractive === undefined){
        HUBU.logger.debug("This ractive has not been rendered.");
      return;
    }

    return _ractive.find(selector);
  };

  self.teardown = function(){
    if(_ractive!==undefined){
      _ractive.teardown();
    }
  };
}
