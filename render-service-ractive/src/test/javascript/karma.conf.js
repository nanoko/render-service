module.exports = function(config) {
    config.set({
        basePath: "${basedir}",
        frameworks: ['jasmine'],
        files: [
            '${project.build.directory}/dependency/**/ractive.min.js',
            '${project.build.directory}/dependency/**/hubu.min.js',
            '${project.build.directory}/dependency/**/render-service.min.js',
            '${project.build.outputDirectory}/assets/${project.artifactId}.min.js',
            'src/test/js/*.js' //the actual test
        ],
        exclude: ['src/test/javascript/karma.conf.js'],
        port: 9876,
        logLevel: config.LOG_INFO,
        browsers: ['PhantomJS'],
        singleRun: true,
        plugins: [
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-junit-reporter'
        ],
        reporters:['progress', 'junit'],
        junitReporter: {
            outputFile: 'target/surefire-reports/karma-test-results.xml',
            suite: ''
        }
    });
};