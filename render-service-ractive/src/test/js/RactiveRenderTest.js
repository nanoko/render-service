const TESTMAX = 10; //Maximum number of tests that run in a loop.

describe("RactiveRender component test suite", function () {
    var rrComp = null; //RactiveRender component
    var rrConf; // default configuration

    beforeEach(function(){
        rrConf = {
            template : "toto",
            container : "toto",
            model : {}
        };
        rrComp = new RactiveRender();
    });

    afterEach(function () {
        hub.reset();
        rrComp = null;
        rrConf = null;
    });


    it("Should start properly with proper configuration", function() {
        //Given (rrComp)

        //When
        hub.registerComponent(rrComp,rrConf);
        hub.start();
     });

    it("Should register a RenderService", function() {
        //Given (rrComp)

        //When
        hub.registerComponent(rrComp,rrConf);
        hub.start();

        //Then
        expect(hub.getServiceReferences(window.RenderService).length).toBe(1);
    });

    it("Should register a RactiveRenderService", function() {
        //Given (rrComp)

        //When
        hub.registerComponent(rrComp,rrConf);
        hub.start();

        //Then
        expect(hub.getServiceReferences(window.RactiveRenderService).length).toBe(1);
    });

});

